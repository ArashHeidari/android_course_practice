package Android.Course.foods_list;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import Android.Course.R;

public class FoodsListActivity extends AppCompatActivity {
    ListView foodsList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foods_list);
        foodsList = findViewById(R.id.foodsList);
        FoodModel pizzaP = new FoodModel();
        pizzaP.setName("Pepperoni");
        pizzaP.setPrice(32000);
        pizzaP.setImage("https://www.cicis.com/media/1243/pizza_adven_zestypepperoni.png");

        FoodModel pizzaM = new FoodModel();
        pizzaM.setName("margherita");
        pizzaM.setPrice(28000);
        pizzaM.setImage("http://storage-cube.quebecormedia.com/v1/cl_prod/canadian_living/f6b9deb0bca458c301eb1825a264364acba975a2/pizza-margherita.jpg");


        FoodModel pizzaS = new FoodModel();
        pizzaS.setName("Steak");
        pizzaS.setPrice(45000);
        pizzaS.setImage("https://tmbidigitalassetsazure.blob.core.windows.net/secure/RMS/attachments/37/1200x1200/exps166687_SD163575B10_08_3b.jpg");

        FoodModel pizzaMa = new FoodModel();
        pizzaMa.setName("Mix");
        pizzaMa.setPrice(33000);
        pizzaMa.setImage("https://irancook.ir/wp-content/uploads/2012/10/pizza-small.jpg");

        List<FoodModel> foods = new ArrayList<>();
        foods.add(pizzaP);
        foods.add(pizzaM);
        foods.add(pizzaS);
        foods.add(pizzaMa);
        foods.add(pizzaP);
        foods.add(pizzaM);
        foods.add(pizzaS);
        foods.add(pizzaMa);
        FoodsAdpater fa=new FoodsAdpater(this, foods);
        foodsList.setAdapter(fa);






    }
}
