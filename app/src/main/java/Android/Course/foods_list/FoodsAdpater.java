package Android.Course.foods_list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import Android.Course.R;

public class FoodsAdpater extends BaseAdapter {
    Context mcontext;
    List<FoodModel> foods;

    public FoodsAdpater(Context mcontext, List<FoodModel> foods) {
        this.mcontext = mcontext;
        this.foods = foods;
    }

    @Override
    public int getCount() {
        return foods.size();
    }

    @Override
    public Object getItem(int position) {
        return foods.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View v = LayoutInflater.from(mcontext).inflate(R.layout.foods_list_item, viewGroup, false );

        TextView name = v.findViewById(R.id.name);
        TextView price = v.findViewById(R.id.price);
        ImageView image = v.findViewById(R.id.image);
        name.setText(foods.get(position).getName());
        price.setText(foods.get(position).getPrice()+"");
        Glide.with(mcontext).load(foods.get(position).getImage()).into(image);
        return v;
    }
}
