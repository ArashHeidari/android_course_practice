package Android.Course;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.orhanobut.hawk.Hawk;

import Android.Course.Utils.BaseActivity;
import Android.Course.Utils.PublicMethods;

public class SharedPreferenceActivity extends BaseActivity {
    EditText username, password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preference);
        bind();
        loadData();
        findViewById(R.id.save).setOnClickListener(V->{
            String uVal = username.getText().toString();
            String pVal = password.getText().toString();
//            PreferenceManager.getDefaultSharedPreferences(this)
//                    .edit().putString("username" , uVal).apply();
//            PreferenceManager.getDefaultSharedPreferences(this)
//                    .edit().putString("password" , pVal).apply();
            PublicMethods.saveData(mcontext,"username",uVal);
            PublicMethods.saveData(mcontext,"password",pVal);
        } );

    }

    void bind(){
        username=findViewById(R.id.username);
        password=findViewById(R.id.password);
    }
    void loadData(){
//        String defU = PreferenceManager.getDefaultSharedPreferences(this).getString("username","" );
//        String defP = PreferenceManager.getDefaultSharedPreferences(this).getString("password","" );
          String defU = PublicMethods.getData(mcontext,"username");
          String defP = PublicMethods.getData(mcontext,"password");
        if(defU.length()>0){
            username.setText(defU);
        }
        if(defP.length()>0){
            password.setText(defP);
        }
    }
}
