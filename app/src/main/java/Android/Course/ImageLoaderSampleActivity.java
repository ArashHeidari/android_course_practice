package Android.Course;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class ImageLoaderSampleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_loader_sample);
        ImageView mainbanner = findViewById(R.id.mainbanner);

        Glide.with(this).load("https://static2.farakav.com/files/pictures/01357372.jpg")
                .into(mainbanner);



    }
}
