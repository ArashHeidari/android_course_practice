package Android.Course;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class AndroidSampleActivity extends AppCompatActivity implements View.OnClickListener {
EditText name,family;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_sample);
        name=findViewById(R.id.name);
        family=findViewById(R.id.family);
        //findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
           // @Override
          //  public void onClick(View v) {

            //}
        //});
        //findViewById(R.id.btn).setOnClickListener(this);

        findViewById(R.id.btn).setOnClickListener(V->{
            String username = name.getText().toString();
            String familyname = family.getText().toString();
            Intent intent = new Intent (this,SecondActivity.class );
            intent.putExtra("name", username);
            intent.putExtra("family",familyname);
            startActivity(intent);
            });
    }

    @Override
    public void onClick(View v) {

    }
}
