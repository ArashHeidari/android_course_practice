package Android.Course;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        TextView txt1 = findViewById(R.id.txt1);
        String username;
        username= getIntent().getStringExtra("name");
        String familyname = getIntent().getStringExtra("family");
        txt1.setText(username + " " + familyname);

    }
}
