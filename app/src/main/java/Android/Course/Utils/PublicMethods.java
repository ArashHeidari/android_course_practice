package Android.Course.Utils;

import android.content.Context;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

public class PublicMethods {

    public static void toast(Context Mcontext, String msg){
        Toast.makeText(Mcontext,msg, Toast.LENGTH_SHORT).show();
    }
    public static void saveData(Context mContext, String key, String val){
        Hawk.put(key, val);
    }
    public static String getData(Context mContext,String key){
       return Hawk.get(key);
    }
}
